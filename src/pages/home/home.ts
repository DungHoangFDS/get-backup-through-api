import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { GooglePlus } from '@ionic-native/google-plus';
import { HTTP } from '@ionic-native/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  private accessToken: any;
  private driveFileId: any = "1Vpr7gF85anJ6XDGD14aoxVKDUXPQJqMl";
  constructor(public navCtrl: NavController
    , private googlePlus: GooglePlus
    // , private http: HTTP,
    , private http: HttpClient,
  ) {

  }
  login() {
    var array = [];
    this.googlePlus.login({})
      .then(res => {
        console.log(JSON.stringify(res));
        console.log("token")
        // console.log(res.accessToken)
        this.accessToken = res.accessToken;
        this.searchFile();
      })
      .catch(err => console.error(err));
  }
  searchFile() {
    // this.http.get('https://www.googleapis.com/drive/v3/files', {
    //   headers: new HttpHeaders().set('Authorization', 'Bearer ' + this.accessToken)
    // }).subscribe(data => {
    //   console.log(JSON.stringify(data));

    //   data['files'].forEach((value, index) => {
    //     if (value.name == 'backup.txt') {
    //       this.driveFileId = value.id;
    //       console.log(value.id);
    //       return;
    //     }
    //   });
      this.retrieveData();
    // });
  }

  retrieveData() {
    this.http.get('https://www.googleapis.com/drive/v3/files/' + this.driveFileId + '?alt=media', {
      headers: new HttpHeaders().set('Authorization', 'Bearer ' + this.accessToken),
      responseType: 'text'
    },).toPromise().then(data => {
      console.log(data)
    }).catch(err => {
      console.log(JSON.stringify(err))
    });
  }
}
